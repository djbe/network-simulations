package com.cpl.netsimTest

object Demo {

  def main(){
    println("Welcome to the demo of this network simulations packet.")
    println("There are a two test scenario's that can be shown.")
    
    println("Press 1 for for the risk (board game) test scenario.")
    println("Press 2 for the viral infection test scenario.")
    
    var choice:Int = Console.readInt()
    
    if(choice == 1){
      TestRisk.main(new Array[String](0))
    }
    else if (choice == 2){
      TestViral.main(new Array[String](0))
    }
    else{
      println("Please enter either 1 or 2.")
    }
  }
}