package com.cpl.netsimTest

import com.cpl.netsim._
import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._
import com.cpl.netsim.simulation._
import views.UbiGraphView

/**
 * Simulates a virus spreading trough a population. A person can undergo multiple stages:
 * healthy
 * incubated - during incubation, a person shows no symptoms but can spread the disease
 * infected
 * dead
 * immune (a cured person becomes immune)
 */
object TestViral {
	var sim: EventBasedSimulation = null
	// Rate at which infected people die
	val dieRate = 0.005
	// Rate at which infected people cure
	val cureRate = 0.01
	//Rate at which infection spreads to other people (for each person in contact with the infected person)
	val infectionRate = 0.01
	//Timespan during which a person is infected but shows no symptoms.
	val incubationTime = 20
	//Is quarantine in place?	
	var quarantine:Boolean = false
	
	

	def main(args: Array[String]) {
	  	var stats = new Statistics
		val view = new UbiGraphView

		view.setAttributeMapper("Infected", viewInfection)
		
		val graph: Graph = GraphGenerator.createRandomGraph(1000, 8000, Bidirectional)
		//val graph: Graph = IO.read("src/files/viralTest.graphml")


//		turnSimulation()
		eventSimulation(graph)

//		Log.print()
//		stats.prettyPrint()
//		stats.terminate()
	}

	def viewInfection(author: GraphElement, value: String): Map[String, String] = {
		value match {
			case "true" => Map("color" -> "#FF0000")
			case "false" => Map("color" -> "#0000FF")
			case _ => Map()
		}
	}

	// ---- Turn based simulation ----

	def turnSimulation() {
		Log.log(TSimulation, "Preparing: Test")
		val graph: Graph = IO.read("src/files/viralTest.graphml")
		val tbs = new TurnBasedSimulation(graph, 4, performActionTestSimulation)
		tbs.setSleep(400)

		graph.getRandomActor().setAttribute("Infected", "true")

		Log.log(TSimulation, "Start: Test")
		tbs.run()
		Log.log(TSimulation, "End: Test")
	}

	def performActionTestSimulation(actor: Actor) {
		if (actor.getAttribute("Infected").equals("true"))
			for (actor2: Actor <- actor.neighbours())
				if (actor2.getAttribute("Infected").equals("false")) {
					actor2.setAttribute("Infected", "true")
					Log.logObject(TActor, actor2, "Infection! \t" + actor2.name + " was infected")
				}
	}

	// ---- Event based simulation ----

	def eventSimulation(graph:Graph) {
		
		for(actor:Actor <- graph.getActors()){
		  actor.setAttribute("Infected", "false")
		  actor.setAttribute("Immune", "false")
		  actor.setAttribute("Quarantine", "false")
		}
	  		
		val nbPeople = graph.actorsCount()
				
		sim = new EventBasedSimulation(graph, 10000)
		//sim.setSleep(400)
		
		val infectionMonitor: InfectionMonitor = new InfectionMonitor(graph, sim)


		new Event(graph.getRandomActor(), "infection", infect, sim, 1)

		sim.run()
		
		println("Ouf of " +nbPeople + " people, " + infectionMonitor.nbInfections + " people were infected.")
		println(infectionMonitor.nbDeaths + " died.")
	}
	
	def infectionDelay(actor:GraphElement): Double = {
		Math.log(1-RNG.random)*(-1/infectionRate)
	}

	def cureDelay(actor:GraphElement): Double = {
		Math.log(1-RNG.random)*(-1/cureRate)
	}

	def dieDelay(actor: GraphElement): Double = {
		Math.log(1-RNG.random)*(-1/dieRate)
	}
	
	def infect(author: GraphElement) {
		author match {
			case actor: Actor =>
				if (actor.getAttribute("Infected").equals("false")
					&& actor.getAttribute("Immune").equals("false")) {

					println(actor.toString() + " got infected!")

					actor.setAttribute("Infected", "true")

					val dieTime:Double = dieDelay(actor)+incubationTime
					val cureTime:Double = cureDelay(actor)+incubationTime

					if(dieTime < cureTime)
						new Event(actor, "die", die, sim, dieTime)
					else
						new Event(actor, "cure", cure, sim, cureTime)
					
									
					for (contact:Link <- actor.getLinks) {
						val infectionTime:Double = infectionDelay(contact)

						if (infectionTime < List(dieTime, cureTime).min)
							new Event(contact, "infection", infect, sim, infectionTime)
					}
					
					if(quarantine){
					  new Event(actor, "quarantine", quarantineActor, sim, incubationTime)
					}

				}
				
				
			case link:Link =>
			  if(link.from.getAttribute("Infected").equals("true") 
			      && link.from.getAttribute("Quarantine").equals("false")){
			    infect(link.to);
			  }
			  else if(link.to.getAttribute("Infected").equals("true")
			      && link.to.getAttribute("Quarantine").equals("false")){
			    infect(link.from)
			  }
		}
	}

	
	def cure(actor: GraphElement) {
		if (actor.getAttribute("Infected").equals("true")) {
			println(actor + " was cured and is now immune!")
			actor.setAttribute("Infected","false")
			actor.setAttribute("Immune", "true")
		}
	}
	
	
	def die(author: GraphElement) {
		author match {
			case actor: Actor =>
				println(actor + " died!")
				actor.graph.removeActor(actor)
			case _ => throw new ClassCastException
		}
	}
	
	def quarantineActor(actor:GraphElement){
	  actor match{
	    case actor:Actor =>
	      println(actor + " in quarantine")
	      actor.setAttribute("Quarantine", "true")
	  }
	}
}



/**
 * An InfectionMonitor is an object that monitors the population, the amount of people
 * that have been infected, and the amount of people that died.
 * Additionally, as soon as a given limit of infections is reached, it will enforce 
 * quarantine on the infected.
 */
class InfectionMonitor(graph:Graph, sim:EventBasedSimulation) extends Monitor{
  val nbPeople = graph.actorsCount()
  var nbDeaths = 0
  
  var nbInfections = 0
  val quarantineLimit = 0.1
 
  override def filter = List(TActor)
  
  override def notify(tag: Tag, time: Int, author: Object, message: String, parameters: Map[String,  String])
  = (tag, message) match{

    case (TActor, "deleted") => nbDeaths = nbDeaths + 1
    case (TActor, "attribute.set") =>
      {
        if(parameters("name") == "Infected"){
          if(parameters("old") == "false" && parameters("new") == "true"){
            nbInfections +=1
            
            if(!TestViral.quarantine && nbInfections > quarantineLimit*nbPeople){
              launchQuarantine()
            }
          }
        }
      }
    case _ =>
  }
  
  def launchQuarantine(){
    TestViral.quarantine = true
    
    for(actor:Actor <- graph.getActors()){
    	new Event(actor, "quarantine", TestViral.quarantineActor, sim, TestViral.incubationTime)
    }
  }
  
  
  
  
}
