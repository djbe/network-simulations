package com.cpl.netsimTest

import com.cpl.netsim._
import com.cpl.netsim.monitoring._
import views.UbiGraphView

/**
 * Created by IntelliJ IDEA.
 * User: davidjennes
 * Date: 17/11/11
 * Time: 08:16
 */

object TestFacebook {
	def main(args: Array[String]) {
		val view = new UbiGraphView

		testFacebook()

//		Log.print()
	}

	private def testFacebook() {
		Log.log(TSimulation, "Preparing: Facebook")
		val graph = IO.read("src/files/facebookTest.graphml")
	}
}