package com.cpl.netsimTest

import com.cpl.netsim._
import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._
import com.cpl.netsim.simulation._
import com.cpl.netsim.views._
import collection.mutable.ListBuffer

object TestRisk {
    val players = List("Fran","Karl","David","Kwinten","Neutral")
	
    def main(args: Array[String]) {
		val stats : Statistics = new Statistics
		val view = new UbiGraphView
		val view2 = new GnuPlotView(("Side", "Fran"), ("Side", "Karl"), ("Side", "David"), ("Side", "Kwinten"))

		view.setAttributeMapper("Side", viewSide)
		view.setAttributeMapper("Power", viewPower)

		testRisk()

		//Log.print()
		stats.prettyPrint()
		stats.terminate()
		view2.show()
	}

	def viewSide(author: GraphElement, value: String): Map[String, String] = {
		value match {
			case "Fran" => Map("color" -> "#0000FF")
			case "Karl" => Map("color" -> "#00FF00")
			case "David" => Map("color" -> "#00FFFF")
			case "Kwinten" => Map("color" -> "#FF0000")
			case "Neutral" => Map("color" -> "#FFFFFF")
			case _ => Map()
		}
	}

	def viewPower(author: GraphElement, value: String): Map[String, String] = {
		if (value != null) {
			if (value.toDouble / 10 < 1.0)
				Map("size" -> "1.0")
			else
				Map("size" -> (value.toDouble / 10).toString)
		} else
			Map()
	}

	private def testRisk() {
		Log.log(TSimulation, "Preparing: Risk")
		val graph = IO.read("src/files/risk.graphml")
    	val tbs = new TurnBasedSimulation(graph, 16, performActionTestRisk, updateRiskBoard)
		tbs.setSleep(250)

		Log.log(TSimulation, "Start: Risk")
		tbs.run()
		Log.log(TSimulation, "End: Risk")
	}

	
	def performActionTestRisk(actor: Actor) {
		if( !actor.getAttribute("Side").equals(players.last)){
		    val enemies : ListBuffer[Actor] = getEnemyPlayers(actor)
		    attackEnemies(actor, enemies)
			if ( enemies.isEmpty){
				moveArmies(actor, getNeighboursOfActorWithSide(actor, actor.getAttribute("Side")))
			}
		}
	}

	private def getEnemyPlayers(actor : Actor) : ListBuffer[Actor] ={
		val enemies : ListBuffer[Actor] = new ListBuffer[Actor]
		for( player : String <- players){
			if(!player.equals(actor.getAttribute("Side")))
	//		    enemies ++= actor.graph.getActorsWithAttributeValue("Side",player)	// dees is ove rheel de map
				enemies ++= getNeighboursOfActorWithSide(actor,player)
		}
	  	RNG.shuffle(enemies)
	}
	
	private def getNeighboursOfActorWithSide( actor : Actor, side : String) : ListBuffer [Actor]= {
	  val neighbours : ListBuffer[Actor] = new ListBuffer[Actor]
		for( neigh <- actor.neighbours())
			if( neigh.getAttribute("Side").equals(side))
				neighbours += neigh
		neighbours
	}
	
	private def moveArmies( actor : Actor, friends : ListBuffer[Actor]){
		var mPower : Int = actor.getAttribute("Power").toInt

		if (mPower > 0) {
			var t = RNG.randomInt(mPower)
			actor.setAttribute("Power", t.toString)
			mPower -= t

			for (friend <- friends)
				if (mPower > 0) {
					val fPower : Int = friend.getAttribute("Power").toInt
					t = RNG.randomInt(mPower)
					friend.setAttribute("Power", (fPower + t).toString)
					mPower -= t
				}
		}
	}
	
	private def attackEnemies( actor : Actor , enemies : ListBuffer[Actor]){
		for( enemy <- enemies){
			attackEnemy(actor,enemy)
		}
	}
	
	private def attackEnemy(actor : Actor, enemy : Actor){
		var ePower : Int = enemy.getAttribute("Power").toInt
		var mPower : Int = actor.getAttribute("Power").toInt
		if( mPower >= ePower){
//		  Log.logObject(TActor, actor, "Invasion! \t" + actor.name + " invades " + enemy)
		  var ePowerAfter : Int = ((mPower - ePower) *0.6).toInt
		  var mPowerAfter : Int = mPower - ((mPower - ePower)*0.6).toInt
		  enemy.setAttribute("Side",actor.getAttribute("Side"))
		  enemy.setAttribute("Power", ePowerAfter.toString)
		  actor.setAttribute("Power", mPowerAfter.toString)
		} else if (RNG.random <= 0.5) {
		  var ePowerAfter : Int = (ePower * 0.6).toInt
		  var mPowerAfter : Int = (mPower * 0.8).toInt
		  enemy.setAttribute("Power", ePowerAfter.toString)
		  actor.setAttribute("Power", mPowerAfter.toString)
		}
	}
	
	private def updateRiskBoard(actor : Actor){
		if ( !actor.getAttribute("Side").equals(players.last))
			actor.setAttribute("Power", (actor.getAttribute("Power").toInt + 2).toString)
	}
}