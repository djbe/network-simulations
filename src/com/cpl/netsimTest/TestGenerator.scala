package com.cpl.netsimTest

import com.cpl.netsim._
import com.cpl.netsim.graph._
import com.cpl.netsim.views.UbiGraphView

object TestGenerator {
	def main(args: Array[String]){
		val view = new UbiGraphView
		val graph: Graph = GraphGenerator.createRandomGraph(30, 100, Bidirectional)

    	IO.write(graph, "output/generatedGraph.graphml")
  	}
}
