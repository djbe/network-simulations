package com.cpl.netsimTest

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._

object Test {
	def main(args: Array[String]) {
		testVaria()
//		Log.print()
	}

	private def testVaria() {
		Log.log(TSimulation, "Start: Varia")

		var graph:Graph = new Graph()
		var actor1:Actor = new Actor(graph, "Calvin")
		var actor2:Actor = new Actor(graph, "Hobbes")

		println(actor1)
		println(actor2)

		if (graph.actorsCount != 2)
			Log.log(TSimulation, "Problem!")

		var link = actor1.createLink(actor2, Unidirectional)

		if (link.from != actor1)
			Log.log(TSimulation, "Problem!")
		if (link.to != actor2)
			Log.log(TSimulation, "Problem!")
		if (link.direction != Unidirectional)
			Log.log(TSimulation, "Problem!")

		println(actor1.neighbours(2))
		link.delete()

		link = actor2.createLink(actor1, Bidirectional)

		println(actor1.neighbours())
		println(actor2.neighbours())

		graph.removeActor(actor1)

		println(actor2.neighbours())

		Log.log(TSimulation, "End: Varia")
	}
}
