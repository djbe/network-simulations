package com.cpl.netsim.monitoring

/**
 * Monitor trait, extend this to receive message notifications for all kinds of events
 */

trait Monitor {
	// constructor
	Log.register(this)

	/**
	 * List of tags that we want to receive
	 */
	def filter(): List[Tag]

	/**
	 * This method gets called when a message we want is logged
	 */
	def notify(tag: Tag, time: Int, author: Object, message: String, parameters: Map[String,  String])

	/**
	 * Terminates the monitor (unregisters with {@link Log})
	 */
	def terminate() {
		Log.unregister(this)
	}
}