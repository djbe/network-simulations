package com.cpl.netsim.monitoring

import collection.immutable.Map

/**
 * All possible message types (tags)
 */
sealed trait Tag
case object TActor extends Tag
case object TEvent extends Tag
case object TGraph extends Tag
case object TIO extends Tag
case object TLink extends Tag
case object TSimulation extends Tag
case object TStatistic extends Tag

/**
 * Log message, contains a time, tag, text message and an authoring object (can be Nil)
 */
class LogMessage(time: Int, authorObject: Object, msg: String, tagObject: Tag, params: Map[String, String]) {
	def timestamp = time;
	def author = authorObject;
	def message = msg;
	def tag = tagObject;
	def parameters = params

	override def toString: String = {
		"[" + timestamp + "] \t" + tag.toString + " \t" + msg + "\t(" + author.toString + ") \t" + params.toString();

	}
}
