package com.cpl.netsim.monitoring

import scala.collection.mutable.ListBuffer
import collection.mutable.HashMap

/**
 * Logging object, everything passes through here
 */

object Log {
	/** A {@link ListBuffer} where all the received {@link LogMessages} are stored */
	private val lines: ListBuffer[LogMessage] = new ListBuffer[LogMessage];
	/** A {@link HashMap} used to notify {@link Monitor}s when a specific {@link LogMessage} is received. */
	private val monitors: HashMap[Tag, ListBuffer[Monitor]] = new HashMap[Tag, ListBuffer[Monitor]]
	/** The sequential time in the log */
	var timestamp: Int = -1;

	/**
	 * Log a message with no objects
	 * @param	tag		The {@link Tag}	log message carries.
	 * @param	msg		The message to be logged.
	 */
	def log(tag: Tag,  msg: String) {
		addLogMessage(Nil, msg, tag, Map())
	}

	/**
	 * Log a message with an author object
	 * @param	tag		The {@link Tag}	log message carries.
	 * @Param	author	The author who created the log message.
	 * @param	msg		The message to be logged.
	 */
	def logObject(tag: Tag, author: Object,  msg: String) {
		addLogMessage(author, msg, tag, Map())
	}

	/**
	 * Log a value for a certain object, parameter keys: "value"
	 */
	def logObjectValue(tag: Tag, author: Object,  msg: String, value: String) {
		addLogMessage(author, msg, tag, Map("value" -> value))
	}

	/**
	 * Log a value change for a certain object, parameter keys: "old" and "new"
	 * @remark not used for now
	 */
	def logObjectChange(tag: Tag, author: Object,  msg: String, oldValue: String,  newValue: String) {
		addLogMessage(author, msg, tag, Map("old" -> oldValue, "new" -> newValue))
	}

	/**
	 * Log a named value change for a certain object, parameter keys: "name", "old" and "new"
	 */
	def logObjectNamedChange(tag: Tag, author: Object,  msg: String, name: String, oldValue: String,  newValue: String) {
		addLogMessage(author, msg, tag, Map("name" -> name, "old" -> oldValue, "new" -> newValue))
	}

	/**
	 * Register a monitor instance so it can receive messages
	 */
	def register(monitor : Monitor) {
		for (tag <- monitor.filter()) {
			if (!monitors.contains(tag))
				monitors(tag) = new ListBuffer[Monitor]

			monitors(tag) += monitor
		}
	}

	/**
	 * Unregister a monitor so it stops receiving messages
	 */
	def unregister(monitor : Monitor) {
	  for (tag <- monitor.filter())
		  if (monitors.contains(tag))	//trivial
			monitors(tag) -= monitor
	}

	// TODO: this should be removed
	// Don't remove this :O It's handy
	def getLogMessage(timestamp: Int): String = {
		if (timestamp < 0 || timestamp >= lines.length)
			"Timestamp not found";
		else
			lines(timestamp).toString();
	}


//	def print() {
//		for (l <- lines)
//			println(l)
//	}

	/**
	 * Actual logging method
	 */
	private def addLogMessage(author: Object, msg: String, tag: Tag, params: Map[String, String]) {
		timestamp += 1;

		val line : LogMessage = new LogMessage(timestamp, author, msg, tag, params)
		lines += line
		notifyMonitors(line)
	}

	/**
	 * Notify all registered monitor objects about a new message, but only if they want it (see filter method in Monitor)
	 */
	private def notifyMonitors(line: LogMessage) {
		if (monitors.contains(line.tag)) 
			for (monitor:Monitor <- monitors(line.tag))
				monitor.notify(line.tag, line.timestamp, line.author, line.message, line.parameters)
	}
}
