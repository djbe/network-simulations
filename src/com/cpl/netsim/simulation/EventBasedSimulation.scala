package com.cpl.netsim.simulation

import com.cpl.netsim.graph._


/**
 * Event based simulation which, instead of going through all the actors over and over again, only executes a set of
 * events at their specified firing time. The simulation will keep on going for as long as there are events or until a
 * specified ending time.
 */

class EventBasedSimulation(g: Graph, maxTime: Double) extends Simulation {
	private var time:Double  = 0
	private val queue: EventQueue = new EventQueue()
	val graph = g

	/**
	 * @see super.run()
	 */
	override def run() {
		while (queue.hasNext() && time <= maxTime) {
			val next: Event = queue.nextEvent()

			time = next.fireTime()
			println("Time: " + time)

			next.fire()
			Thread.sleep(sleep)
		}
	}

	/**
	 * Add an event to the queue
	 */
	def addEvent(event:Event) {
		queue.addEvent(event)
	}
	
	/**
	 * Indicates the current time as in the simulation.
	 */
	def getTime() = time
}
