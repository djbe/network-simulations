package com.cpl.netsim.simulation

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._

/**
 * An event is the cornerstone of event-based simulation. 
 * An event consist of a time and a method to fire. Both are supplied by the user.
 * When the indicated time is reached, the method is fired.
 */
class Event(element: GraphElement, name: String, fireMethod: (GraphElement) => Unit, sim: EventBasedSimulation, delay: Double) {
	// constructor
	val fireT: Double = sim.getTime() + delay
	sim.addEvent(this)

	/**
	 * Actual fire time
	 */
	def fireTime() = fireT

	/**
	 * Fire event
	 */
	def fire() {
		Log.logObject(TEvent, element, name)
		fireMethod(element)
	}
	
}

object EventOrdering extends Ordering[Event]{
  	override def compare(event1:Event, event2:Event):Int ={	  
	  if( event1.fireTime() < event2.fireTime()){
	    return 1
	  }
	  else {
	    return -1
	  }
	}
}