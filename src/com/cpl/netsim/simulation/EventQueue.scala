package com.cpl.netsim.simulation

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.PriorityQueue

/**
 * Event queue, events are ordered by fire time
 */

class EventQueue {
	// the events list should be ordered at all times - earlier events first
	//TODO: efficient implementation
	var events:PriorityQueue[Event] = new PriorityQueue[Event]()(EventOrdering) //ListBuffer[Event] = new ListBuffer[Event]()

	/**
	* Removes and returns the next event
	*/
	def nextEvent(): Event ={
		val event: Event = events.dequeue()

		event
	}

	/**
	* Add an event at the correct place in the queue
	*/
	def addEvent(event:Event){
		events. += (event)
	}

	/**
	 * Check if the queue is empty
	 */
	def hasNext(): Boolean = {
		events.size != 0
	}
}
