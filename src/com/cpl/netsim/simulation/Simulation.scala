package com.cpl.netsim.simulation

import com.cpl.netsim.graph.Graph

/**
 * Common simulation trait
 */

trait Simulation {
	protected var sleep: Int = 0
	def graph: Graph


	/**
	 * Execute the simulation
	 */
	def run()

	/**
	 * Set the amount of time to sleep after each action/event/... Needed to watch the simulation occur at a slower rate
	 */
	def setSleep(s: Int) {
		sleep = s
	}
}
