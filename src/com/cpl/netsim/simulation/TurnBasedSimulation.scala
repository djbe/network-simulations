package com.cpl.netsim.simulation

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._

/**
 * Simple turn based simulation which, for a number of rounds, goes through all the actors and applies the specified
 * action to it. Should you specify multiple action functions, the simulator will first apply the first action on all
 * actors, then the 2nd action on all actors, and so on.
 */

sealed trait TraversalMode
case object Naive extends TraversalMode
case object BreadthFirst extends TraversalMode
case object DepthFirst extends TraversalMode

class TurnBasedSimulation[T](g: Graph, nbRounds: Int, performActions: (T => Unit)*) extends Simulation {
    private var round: Int = 1
	var traversalMode: TraversalMode = Naive
	val graph = g
  
	/**
	 * @see super.run()
	 */
  	override def run() {
	    while (round <= nbRounds) {
      		Log.logObjectValue(TSimulation, this, "round", round.toString)

			for (performAction <- performActions)
				performAction match {
					case actorAction: (Actor => Unit) =>
						performActorAction(actorAction)
					case linkAction: (Link => Unit) =>
						for (link: Link <- graph.getLinks())
							linkAction(link)
					case _ =>
				}
					
      		round += 1
			Thread.sleep(sleep)
    	}
  	}

	private def performActorAction(action: (Actor => Unit)) {
		var traversalList = graph.getActors()

		traversalMode match {
			case BreadthFirst =>
				traversalList = graph.getBreadthFirstTraversal(traversalList(0))
			case DepthFirst =>
				traversalList = graph.getDepthFirstTraversal(traversalList(0))
			case Naive =>
		}

		for (actor: Actor <- traversalList)
			action(actor)
	}
}
