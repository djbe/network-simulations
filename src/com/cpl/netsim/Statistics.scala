package com.cpl.netsim

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer


/**
 * Class which contains a general overview of the simulation.
 */
class Statistics extends Monitor {
    private var eventCutOff : Int = 20
    private var eventNumber : Int = 0
    
	private var nbOfActors: Int = 0
	private var nbOfLinks: Int = 0
	private var simulationRound: Int = 0
	

	private val listOfNodeAttributes: HashMap[String,HashMap[String,Int]] = new HashMap[String,HashMap[String,Int]]
	private val listOfLinkAttributes: HashMap[String,HashMap[String,Int]] = new HashMap[String,HashMap[String,Int]]
	private val listOfActorsInGraph: ListBuffer[String] = new ListBuffer[String]
	private val listOfLinksInGraph: ListBuffer[String] = new ListBuffer[String]
	private val nbOfNeighboursForEachNode: HashMap[String, Int] = new HashMap[String, Int]
	
	//lists to calculate rates
	private var listInRoundsOfActorsJoined : ListBuffer[Int] = new ListBuffer[Int]	//index = round; value = #joins
	private var listInRoundsOfActorsLeft : ListBuffer[Int] = new ListBuffer[Int]
	private var listInRoundsOfLinksJoined : ListBuffer[Int] = new ListBuffer[Int]
	private var listInRoundsOfLinksLeft : ListBuffer[Int] = new ListBuffer[Int]
	

	private val listInRoundsOfNodeAttributesChangesTo : ListBuffer[HashMap[String,HashMap[String, Int]]] = new ListBuffer[HashMap[String,HashMap[String, Int]]]
	private val listInRoundsOfNodeAttributesChangesFrom : ListBuffer[HashMap[String,HashMap[String, Int]]] = new ListBuffer[HashMap[String,HashMap[String, Int]]]
	private val listInRoundsOfLinkAttributesChangesTo : ListBuffer[HashMap[String,HashMap[String, Int]]] = new ListBuffer[HashMap[String,HashMap[String, Int]]]
	private val listInRoundsOfLinkAttributesChangesFrom : ListBuffer[HashMap[String,HashMap[String, Int]]] = new ListBuffer[HashMap[String,HashMap[String, Int]]]
  
	override def filter() : List[Tag] = {
		List(TActor, TLink, TSimulation )
	}
	
	/**
	 * Method which is called to update statistics.
	 * @param 	tag 	The {@link Tag} of the message.
	 * @param	author	The author of the message.
	 * @param	message	The message used to update the statistics.
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,... 
	 */
	override def notify(tag: Tag, time: Int, author: Object, message: String, params: Map[String,  String]) {
		tag match {
			case TActor => actorTagOccured(message, author, params)
			case TLink => linkTagOccured(message, author, params)
			case TSimulation => simulationTagOccured(message, params)
			case TEvent => eventTagOccured()
			case _ => //TStatistic & TGraph & TIO
		}
	}

	/**
	 * Method called when the {@link Tag} of the {@link Actor} class occurs.
	 * @param	author	The author of the message.
	 * @param	message	The message used to update the statistics.
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,...  
	 */
	private def actorTagOccured(message : String, author: Object, params: Map[String,  String]) {
		author match {
  			case actor: Actor =>
				message match {
					case "created" =>
						nbOfActors +=1
						listOfActorsInGraph += actor.toString
						nbOfNeighboursForEachNode(actor.toString) = 0
						if(!listInRoundsOfActorsJoined.contains(simulationRound))
						  listInRoundsOfActorsJoined += 0
						listInRoundsOfActorsJoined(simulationRound) +=1
					case "deleted" =>
						nbOfActors -=1
						listOfActorsInGraph -= actor.toString()
						nbOfNeighboursForEachNode -= actor.toString
						if(!listInRoundsOfActorsLeft.contains(simulationRound))
						  listInRoundsOfActorsLeft += 0
						listInRoundsOfActorsLeft(simulationRound) +=1
						for ((attKey, attValue) <- actor.getAttributes()){
						  listOfNodeAttributes(attKey)(attValue) -= 1
						  
						if(listInRoundsOfNodeAttributesChangesFrom(simulationRound)(attKey).contains(attValue))
							listInRoundsOfNodeAttributesChangesFrom(simulationRound)(attKey)(attValue) +=1
						sendLogMessage(attKey,"percentage.change",attValue,
								    ((listOfNodeAttributes(attKey)(attValue).toDouble/nbOfActors)*100).toInt.toString(), 
								    (((listOfNodeAttributes(attKey)(attValue)-1).toDouble/nbOfActors)*100).toInt.toString() )
						}
						  
					case "attribute.set" =>
						workSetAttribute(params, listOfNodeAttributes)
						updateListInRoundsOfAttributeChanges(params("name"),params("new"),listInRoundsOfNodeAttributesChangesTo)
						updateListInRoundsOfAttributeChanges(params("name"),params("old"),listInRoundsOfNodeAttributesChangesFrom)
						
					case "attribute.remove" =>
						workRemoveAttribute(actor, params, listOfNodeAttributes)
					case _ =>
				}
			case _ => throw new ClassCastException
		}
	}


	/**
	 * Method called when the {@link Tag} of the {@link Link} class occurs.
	 * @param	author	The author of the message.
	 * @param	message	The message used to update the statistics.
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,...  
	 */
	private def linkTagOccured(message : String, author: Object, params: Map[String,  String]){
		author match {
  			case link: Link =>
				message match {
					case "created" =>
						nbOfLinks += 1
						listOfLinksInGraph += author.toString()
						nbOfNeighboursForEachNode(link.from.toString) += 1
						nbOfNeighboursForEachNode(link.to.toString) += 1
						if(!listInRoundsOfLinksJoined.contains(simulationRound))
						  listInRoundsOfLinksJoined += 0
						listInRoundsOfLinksJoined(simulationRound) += 1
					case "deleted" =>
						nbOfLinks -= 1
						listOfLinksInGraph -= author.toString()
						nbOfNeighboursForEachNode(link.from.toString) -= 1
						nbOfNeighboursForEachNode(link.to.toString) -= 1
						if(!listInRoundsOfLinksLeft.contains(simulationRound))
						  listInRoundsOfLinksLeft += 0
						listInRoundsOfLinksLeft(simulationRound) += 1
						for ((attKey, attValue) <- link.getAttributes()){
						  listOfLinkAttributes(attKey)(attValue) -= 1
						
						if(listInRoundsOfLinkAttributesChangesFrom(simulationRound)(attKey).contains(attValue))  
							listInRoundsOfLinkAttributesChangesFrom(simulationRound)(attKey)(attValue) +=1
						sendLogMessage(attKey,"percentage.change",attValue,
								    ((listOfLinkAttributes(attKey)(attValue).toDouble/nbOfActors)*100).toInt.toString(), 
								    (((listOfLinkAttributes(attKey)(attValue)-1).toDouble/nbOfActors)*100).toInt.toString() )
						}
					case "attribute.set" =>
						workSetAttribute(params, listOfLinkAttributes)
						updateListInRoundsOfAttributeChanges(params("name"),params("new"),listInRoundsOfLinkAttributesChangesTo)
						updateListInRoundsOfAttributeChanges(params("name"),params("old"),listInRoundsOfLinkAttributesChangesFrom)
					case "attribute.remove" =>
						workRemoveAttribute(link, params, listOfLinkAttributes)
					case _ =>
				}
  			case _ => throw new ClassCastException
		}
	}
	
	/**
	 * Method called when the {@link Tag} of the {@link Simulation} class occurs.
	 * @param	message	The message used to update the statistics.
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,...
	 */
	private def simulationTagOccured(message : String, params: Map[String,  String]) {
		message match {
			case "round" => 
			  		simulationRound = params("value").toInt
			  		listInRoundsOfActorsJoined += 0
					listInRoundsOfActorsLeft += 0
					listInRoundsOfLinksJoined += 0
					listInRoundsOfLinksLeft += 0
					listInRoundsOfNodeAttributesChangesTo += new HashMap[String,HashMap[String, Int]]
		  			listInRoundsOfNodeAttributesChangesFrom += new HashMap[String,HashMap[String, Int]]
		  			listInRoundsOfLinkAttributesChangesTo += new HashMap[String,HashMap[String, Int]]
		  			listInRoundsOfLinkAttributesChangesFrom += new HashMap[String,HashMap[String, Int]]
			case _ =>
		}
	}

	/**
	 * Method called when the {@link Tag} of the {@link Event} class occurs.
	 * @param	message	The message used to update the statistics.
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,...
	 */
	private def eventTagOccured() {
  	    eventNumber += 1
	  if(eventNumber % eventCutOff == 0){
  		simulationRound += 1
  		
  		listInRoundsOfActorsJoined += 0
		listInRoundsOfActorsLeft += 0
		listInRoundsOfLinksJoined += 0
		listInRoundsOfLinksLeft += 0
		listInRoundsOfNodeAttributesChangesTo += new HashMap[String,HashMap[String, Int]]
  		listInRoundsOfNodeAttributesChangesFrom += new HashMap[String,HashMap[String, Int]]
  		listInRoundsOfLinkAttributesChangesTo += new HashMap[String,HashMap[String, Int]]
  		listInRoundsOfLinkAttributesChangesFrom += new HashMap[String,HashMap[String, Int]]
	  }
	}
	
	/**
	 * Method called when an attribute is chanced. It updates the appropriate statistic variables 
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,...  
	 * @param	variableToUpdate	A {@link HashMap} which contains the attribute that chances together it values.
	 */
	private def workSetAttribute(params: Map[String,  String], variableToUpdate: HashMap[String, HashMap[String, Int]]) {
		val newValue = params("new")
		val oldValue = params("old")
		val attribute = params("name")

		if (!variableToUpdate.contains(attribute))
			variableToUpdate(attribute) = new HashMap[String,Int]

		//new Value
		if (!variableToUpdate(attribute).contains(newValue))
			variableToUpdate(attribute)(newValue) = 0
		variableToUpdate(attribute)(newValue) += 1

		//old value
		if(variableToUpdate(attribute).contains(oldValue) )
			variableToUpdate(attribute)(oldValue) -= 1
		sendLogMessage(attribute,"percentage.change",newValue,
				    (((variableToUpdate(attribute)(newValue)-1).toDouble/nbOfActors)*100).toInt.toString(), 
				    ((variableToUpdate(attribute)(newValue).toDouble/nbOfActors)*100).toInt.toString() )
		if ( variableToUpdate(attribute).contains(oldValue))   
			sendLogMessage(attribute,"percentage.change",oldValue,
				    (((variableToUpdate(attribute)(oldValue) +1).toDouble/nbOfActors)*100).toInt.toString(), 
				    ((variableToUpdate(attribute)(oldValue).toDouble/nbOfActors)*100).toInt.toString() )
	}
	
	/**
	 * Method called to update the <variableToUpdate> lists for the current round
	 * @param	attribute	The attribute that changed.
	 * @param 	value 	The new value of <attribute>.
	 * @param	variabletoUpdate 	The list that contains the specific changes for every value from every attribute for each round.
	 */
	private def updateListInRoundsOfAttributeChanges(attribute :String, value : String, variableToUpdate : ListBuffer[HashMap[String,HashMap[String, Int]]]){
		//ListBuffer[HashMap[String,HashMap[String, Int]]]
	
	//	if (!variableToUpdate.contains(simulationRound)){
		if (variableToUpdate.length < simulationRound || (simulationRound == 0 && variableToUpdate.length < 1))
		  variableToUpdate /*(simulationRound) =*/ += new HashMap[String,HashMap[String, Int]]
		if (!variableToUpdate(simulationRound).contains(attribute))
		  variableToUpdate(simulationRound)(attribute) = new HashMap[String, Int]
		
		if ( !variableToUpdate(simulationRound)(attribute).contains(value))
		  variableToUpdate(simulationRound)(attribute)(value) = 0
		  
		variableToUpdate(simulationRound)(attribute)(value) += 1
	}

	/**
	 * Method called when an attribute is removed. It updates the appropriate statistic variables
	 * @param 	author	The author responsible of the attribute chance. 
	 * @param	params	Objects belonging to the specific notification, ex: name, new value,...
	 * @param	variableToUpdate	A {@link HashMap} which contains the attribute that chances together it values.
	 */
	def workRemoveAttribute(author: GraphElement, params: Map[String, String], variableToUpdate: HashMap[String, HashMap[String, Int]]) {
		val attribute = params("value")
		val oldValue = author.getAttribute(attribute)

		variableToUpdate(attribute)(oldValue) -= 1
		
		sendLogMessage(attribute,"percentage.change",oldValue,
		    ((variableToUpdate(attribute)(oldValue).toDouble/nbOfActors)*100).toInt.toString(), 
		    (((variableToUpdate(attribute)(oldValue)+1).toDouble/nbOfActors)*100).toInt.toString() )
	}
	
	private def sendLogMessage(attribute : String , message : String, attributeValue : String, oldValue : String, newValue : String){
	  Log.logObjectNamedChange(TStatistic, attribute, message, attributeValue, oldValue, newValue)
//Debug
//	  if(attribute.equals("Side") &&  ( attributeValue.equals("Kwinten") ))
//	      println("percentage of "+ attributeValue + " : " + oldValue + " -> " + newValue)
	}
	
	/**
	 * Returns the density as {@link Double} of the graph.
	 */
	def getDensity() : Double = {
		(getTotalNumberOfNeighbours() / ((getNbOfActors() - 1) * getNbOfActors()))
	}

	/**
	 * Returns the sum as {@link Int} of all neighbours present in the graph.
	 */
	private def getTotalNumberOfNeighbours(): Int = {
		var totalNumberOfNeighbours : Int = 0

		for ((actor : String , numberOfNeighbours : Int) <- getNbOfNeighboursForEachNode())
			totalNumberOfNeighbours += numberOfNeighbours

		totalNumberOfNeighbours
	}

	/**
	 * Returns a sorted {@link ListBuffer} of {@link Actor} sorted on number of neighbours
	 */
	//TODO: Fran kdenk da je iets bent vergeten to committen; tgeeft errors :P
//	def getListOfActorSortedByMostConnected() : ListBuffer[Actor] = {
//	  val mostConnected : ListBuffer[Actor] = getListOfActorsInGraph() sort(_.neighbours().length > _.neighbours().length)  
//	}

	
	/**
	 * Returns the total number of current {@link Actor}s present in the simulation. 
	 */
	def getNbOfActors() : Int ={ nbOfActors }
	/**
	 * Returns the total number of current {@link Link}s present in the simulation. 
	 */
	def getNbOfLinks() : Int ={ nbOfLinks }
	/**
	 * Returns which round the simulation is in.
	 */
	def getSimulationRound() :Int ={ simulationRound }
	/**
	 * Returns a {@link HashMap} containing the different tags of an {@link Actor}, together with it's values and how many times that value occurs. 
	 */
	def getListOfNodeAttributes() : HashMap[String,HashMap[String,Int]] ={ listOfNodeAttributes }
	/**
	 * Returns a {@link HashMap} containing the different tags of an {@link Link}, together with it's values and how many times that value occurs. 
	 */
	def getListOfLinkAttributes() : HashMap[String,HashMap[String,Int]] ={ listOfLinkAttributes }
	/**
	 * Returns a {@link ListBuffer} containing all the {@link Actor}s as a {@link String} present in the {@link Graph}. 
	 */
	def getListOfActorsInGraph() : ListBuffer[String] ={ listOfActorsInGraph }
	/**
	 * Returns a {@link ListBuffer} containing all the {@link Link}s as a {@link String} present in the {@link Graph}. 
	 */
	def getListOfLinksInGraph() : ListBuffer[String] ={ listOfLinksInGraph }
	/**
	 * Returns a {@link HashMap} containing the {@link Actor}s as a {@link String}, together with the number of {@link Link}s it contains.
	 */
	def getNbOfNeighboursForEachNode() : HashMap[String, Int] ={ nbOfNeighboursForEachNode }
	/**
	 * Returns a {@link ListBuffer} containing how many actors have entered the system each round. 
	 */
	def getActorsJoined() : ListBuffer[Int] ={
	  listInRoundsOfActorsJoined
	}
	/**
	 * Returns a {@link ListBuffer} containing how many {@link Actor}s have left the system each round. 
	 */
	def getActorsLeft() : ListBuffer[Int] ={
	  listInRoundsOfActorsLeft
	}
	/**
	 * Returns a {@link ListBuffer} containing how many {@link Link}s have entered the system each round. 
	 */
	def getLinksJoined() : ListBuffer[Int] ={
	  listInRoundsOfLinksJoined
	}
	/**
	 * Returns a {@link ListBuffer} containing how many {@link Link}s have left the system each round. 
	 */
	def getLinksLeft() : ListBuffer[Int] ={
	  listInRoundsOfLinksLeft
	}
	/**
	 * Returns a {@link ListBuffer} with a {@link HashMap} of attributes which also contains {@link HashMap} containing the values of the attribute and how many times the value has been changed to each round.
	 * @Return	 
	 * 			structure:
	 *				- ListBuffer, contains the rounds
	 *					- HashMap, maps the attribute names
	 *						- HashMap, maps the attribute values to how many times they have been set to for each round.
	 */
	def getNodeAttributesChangedTo() : ListBuffer[HashMap[String,HashMap[String, Int]]] ={
	  listInRoundsOfNodeAttributesChangesTo
	}
	/**
	 * @see getNodeAttributesChangedTo(); instead of "been changed to" it is "been changed from" 
	 */
	def getNodeAttributesChangedFrom() : ListBuffer[HashMap[String,HashMap[String, Int]]] ={
	  listInRoundsOfNodeAttributesChangesFrom
	}
	/**
	 * @see getNodeAttributesChangedTo(); instead of {@link Actor} it is {@link Link} 
	 */	
	def getLinkAttributesChangedTo() : ListBuffer[HashMap[String,HashMap[String, Int]]] ={
	  listInRoundsOfLinkAttributesChangesTo
	}
	/**
	 * @see getNodeAttributesChangedTo(); instead of {@link Actor} it is {@link Link} 
	 * 										and instead of "been changed to" it is "been changed from"
	 */	
	def getLinkAttributesChangedFrom() : ListBuffer[HashMap[String,HashMap[String, Int]]] ={
	  listInRoundsOfLinkAttributesChangesFrom
	}
	/**
	 * Returns the amount of {@link Actor} that joined in <round>
	 * @param round The round of the simulation 
	 */
	def getNbOfActorsJoiningInRound( round : Int ) : Int ={
	  listInRoundsOfActorsJoined(round)
	}
	/**
	 * Returns the amount of {@link Actor} that left in <round>
	 * @param round The round of the simulation 
	 */
	def getNbOfActorsLeavingInRound( round : Int ) : Int ={
	  listInRoundsOfActorsLeft(round)
	}
	/**
	 * Returns the amount of {@link Link} that joined in <round>
	 * @param round The round of the simulation 
	 */
	def getNbOfLinksJoiningInRound( round : Int ) : Int ={
	  listInRoundsOfLinksJoined(round)
	}
	/**
	 * Returns the amount of {@link Link} that left in <round>
	 * @param round The round of the simulation 
	 */
	def getNbOfLinksLeavingInRound( round : Int ) : Int ={
	  listInRoundsOfLinksJoined(round)
	}
	
	/**
	 * Returns a {@link HashMap} containing the attributes linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed to that value in <round>
	 */
	def getNodeAttributesChangedToInRound( round : Int )  : HashMap[String,HashMap[String, Int]] ={
	  listInRoundsOfNodeAttributesChangesTo(round)
	}
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Actor} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed from that value in <round>
	 */
	def getNodeAttributesChangedFromInRound( round : Int )  : HashMap[String,HashMap[String, Int]] ={
	  listInRoundsOfNodeAttributesChangesFrom(round)
	}
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Link} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed to that value in <round>
	 */
	def getLinkAttributesChangedToInRound( round : Int )  : HashMap[String,HashMap[String, Int]] ={
	  listInRoundsOfLinkAttributesChangesTo(round)
	}
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Link} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed from that value in <round>
	 */
	def getLinkAttributesChangedFromInRound( round : Int )  : HashMap[String,HashMap[String, Int]] ={
	  listInRoundsOfLinkAttributesChangesFrom(round)
	}
	
	/**
	 * Returns a the average per round as {@link Double} of {@link Actor} entering the system
	 */	
	def getAverageOfActorsJoining() : Double ={	//gemiddelde per ronde
		getAverageOfList(listInRoundsOfActorsJoined)
	}
	/**
	 * Returns a the average per round as {@link Double} of {@link Actor} leaving the system
	 */
	def getAverageOfActorsLeaving() : Double ={	//gemiddelde per ronde
	  getAverageOfList(listInRoundsOfActorsLeft)
	}
	/**
	 * Returns a the average per round as {@link Double} of {@link Link} entering the system
	 */
	def getAverageOfLinksJoining() : Double ={	//gemiddelde per ronde
	  getAverageOfList(listInRoundsOfLinksJoined)
	}
	/**
	 * Returns a the average per round as {@link Double} of {@link Link} leaving the system
	 */
	def getAverageOfLinksLeaving() : Double ={	//gemiddelde per ronde
	  getAverageOfList(listInRoundsOfLinksLeft)
	}
	
	/**
	 * Returns a the average of the values from the <list> as {@link Double}
	 * @param list	The list which the average is taken
	 */
	private def getAverageOfList( list : ListBuffer[Int]) : Double ={
	  var total : Double = 0
	  for( r : Int <- list)
	    total += r
	  if ( list.length == 0 ) 
		total
	  else
		total / list.length
	}
	
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Node} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed to that value for each round
	 */
	def getAverageOfNodeAttributesChangedTo() : HashMap[String,HashMap[String,Double]] ={
	  getAverageOfAttributesChangedOfListOfHashes(listInRoundsOfNodeAttributesChangesTo)
	}
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Node} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed from that value for each round
	 */	
	def getAverageOfNodeAttributesChangedFrom() : HashMap[String,HashMap[String,Double]] ={
	  getAverageOfAttributesChangedOfListOfHashes(listInRoundsOfNodeAttributesChangesFrom)
	}
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Link} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed to that value for each round
	 */	
	def getAverageOfLinkAttributesChangedTo() : HashMap[String,HashMap[String,Double]] ={
	  getAverageOfAttributesChangedOfListOfHashes(listInRoundsOfLinkAttributesChangesTo)
	}
	/**
	 * Returns a {@link HashMap} containing the attributes of {@link Link} linked to a {@link HashMap} of the values of the attribute and how many times that the attribute has been changed from that value for each round
	 */
	def getAverageOfLinkAttributesChangedFrom() : HashMap[String,HashMap[String,Double]] ={
	  getAverageOfAttributesChangedOfListOfHashes(listInRoundsOfLinkAttributesChangesFrom)
	}
	
	/**
	 * Returns a {@link HashMap} containing the attributes linked to a {@link HashMap} of the values of the attribute and how many times that the value of attribute has been changed for each round
	 */
	private def getAverageOfAttributesChangedOfListOfHashes( listOfHashes : ListBuffer[HashMap[String,HashMap[String, Int]]]) : HashMap[String,HashMap[String,Double]] ={	//per attribute een average
	  //variable to work with:  ListBuffer[HashMap[String,HashMap[String, Int]]]  (= round(attribute)(value) = #times)
	  val result = new HashMap[String,HashMap[String,Double]]
	  //First: Counting total changes
	  for(round : HashMap[String,HashMap[String,Int]] <- listOfHashes){						//round
	    for( (attribute : String, values : HashMap[String,Int] ) <- round){					//attribute
	      if(!result.contains(attribute))
	        result(attribute) = new HashMap[String, Double]
	      for( (value : String, times : Int ) <- values){									//value of attribute
	        if(!result(attribute).contains(value))
	          result(attribute)(value) = 0
	        result(attribute)(value) += times
	      }
	    }
	  }
	  //Second: Devision by rounds (=average)
	  for( (attribute : String, values : HashMap[String,Double]) <- result){
	    for( (value : String, totalDummy : Double) <- values){
	      result(attribute)(value) /= (simulationRound + 1)
	    }
	  }
	  result
	}
	
	
	
	//TODO: unit zetten bij printout
	//TODO: not so pretty at this moment
	def prettyPrint(){ 
	  	println("\nStats\n-----")
	    println("Round of Simulation: " + getSimulationRound())
	    println("Number of Actors: " + getNbOfActors())
	    println("Number of Links: " + getNbOfLinks())
	    println("Number of NodeAttributes present: " + getListOfNodeAttributes().size)
	    println("Number of LinkAttributes present: " + getListOfLinkAttributes().size)
		println("\nActors in Graph with number of neighbours\n-----------------------------------------")
		for(actor : String <- getListOfActorsInGraph() )
			println("\t" + actor + " : \t" +  getNbOfNeighboursForEachNode()(actor))
		println("\nNode Attributes present\n-----------------------")
		for((attKey,attValue)  <- getListOfNodeAttributes()){
		  println("\tAttribute: " + attKey + "  ( value -> # times present)")
		  for((value,times) <- attValue)
		    println("\t\t" + value + "\t->\t" + times)
		}
		println("\nLinks in Graph\n--------------")
		for(link : String <- getListOfLinksInGraph() )
			println("\t" + link)
		println("\nLink Attributes present\n-----------------------")
		for((attKey,attValue)  <- getListOfLinkAttributes()){
		  println("\tAttribute: " + attKey + "  ( value -> # times present)")
		  for((value,times) <- attValue)
		    println("\t\t" + value + "\t->\t" + times)
		}
			
			
		println("\nMore Stats\n----------------------")
		println("Avarage actors created each round: " + getAverageOfActorsJoining() + " actors/round")
		println("avarage actors left each round: " + getAverageOfActorsLeaving() + " actors/round")
		println("avarage links created each round: " + getAverageOfLinksJoining() + " links/round")
		println("avarage links left each round: " + getAverageOfLinksLeaving() + " links/round")
		
		print("\nList of # Actors created per round: (")
		for( nb <- getActorsJoined()) print(nb +", ") 
		println("end)")
		print("List of # Actors left per round:    (")
		for( nb <- getActorsLeft()) print(nb +", ") 
		println("end)")
		
		print("List of # Links joined per round:   (")
		for( nb <- getLinksJoined()) print(nb +", ") 
		println("end)")
		print("List of # Links left per round:     (")
		for( nb <- getLinksLeft()) print(nb +", ") 
		println("end)")
		
		println("\nList of Node Attributes with the amount a value got adapted to for each round: ")
		for (round <- 0 to simulationRound){
		  println("\tround: "+round)
		  for((attKey,attValue) <- getNodeAttributesChangedTo()(round)) 
			  println("\t  "+attKey+" : "+attValue)
		}
		println("\nList of Node Attributes with the amount a value got taken from for each round: ")
		for (round <- 0 to simulationRound){
		  println("\tround: "+round)
		  for((attKey,attValue) <- getNodeAttributesChangedFrom()(round)) 
			  println("\t  "+attKey+" : "+attValue)
		}
		println("\nList of Link Attributes with the amount a value got adapted to for each round: ")
		for (round <- 0 to simulationRound){
		  println("\tround: "+round)
		  for((attKey,attValue) <- getLinkAttributesChangedTo()(round)) 
			  println("\t  "+attKey+" : "+attValue)
		}
		println("\nList of Link Attributes with the amount a value got taken from for each round: ")
		for (round <- 0 to simulationRound){
		  println("\tround: "+round)
		  for((attKey,attValue) <- getLinkAttributesChangedFrom()(round)) 
			  println("\t  "+attKey+" : "+attValue)
		}
		
		println("\nThe average times a value of an Node Attribute had changed TO: ")
		for((attKey,attValue) <- getAverageOfNodeAttributesChangedTo()) 
			  println("\t  "+attKey+" : "+attValue)
		println("\nThe average times a value of an Node Attribute had changed FROM: ")
		for((attKey,attValue) <- getAverageOfNodeAttributesChangedFrom()) 
			  println("\t  "+attKey+" : "+attValue)
		println("\nThe average times a value of an Link Attribute had changed TO: ")
		for((attKey,attValue) <- getAverageOfLinkAttributesChangedTo()) 
			  println("\t  "+attKey+" : "+attValue)
		println("\nThe average times a value of an Link Attribute had changed FROM: ")
		for((attKey,attValue) <- getAverageOfLinkAttributesChangedFrom()) 
			  println("\t  "+attKey+" : "+attValue)
	}
	
	/**
	 * Sets the cutoff of en eventbased simulation (#events / round)
	 * @param nbOfrounds	The number of events to complete one round 
	 */
	def setEventCutOff(nbOfRounds : Int) {
	  if( nbOfRounds > 0)
		  eventCutOff = nbOfRounds
	}
}
