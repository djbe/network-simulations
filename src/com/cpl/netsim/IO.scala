package com.cpl.netsim

/**
 * Object for reading and writing GraphML files
 * @author David Jennes
 */

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._
import scala.xml._
import collection.mutable.{Queue, HashMap}

object IO {
	/**
	 * Read a GraphML file and load it's contents
	 * @return The created Graph object
	 */
	def read(file: String): Graph = {
		val data = XML.loadFile(file)
		val actors = new HashMap[String,  Actor]
		val attributes = new HashMap[String,  String]
		val graph = new Graph

		// read attribute names
		for (val key <- data \\ "key")
			attributes((key \ "@id").text) = (key \ "@attr.name").text

		// get graph element
		val graphElem = data \ "graph"
		val graphDirection = if ((graphElem \ "@edgedefault").text.equals("directed")) Unidirectional else Bidirectional

		// read actors
		for (val node <- graphElem \\ "node") {
			val id = (node \ "@id").text
			val name = (node \\ "NodeLabel").text

			// create actor
			val actor = new Actor(graph, name)
			actors(id) = actor

			readAttributes(node, attributes, actor)
		}

		// read links
		for (val edge <- graphElem \\ "edge") {
			val from = (edge \ "@source").text
			val to = (edge \ "@target").text
			val directed = (edge \ "@directed").text

			// if direction isn't set, use graph direction
			val dir = if (directed.isEmpty) graphDirection else if (directed.toBoolean) Unidirectional else Bidirectional

			// check that actors exist
			if (actors.contains(from) && actors.contains(to)) {
				val link = new Link(graph, actors(from), actors(to), dir)

				readAttributes(edge, attributes, link)
			} else
				Log.log(TIO, "Unable to create edge from " + from + " to " + to + ", unknow actor")
		}

		graph
	}

	/**
	 * Write a Graph instance to a GraphML file
	 */
	def write(graph: Graph, file: String) {
		val attributeNames = new HashMap[String,  String]
		val actorIDs = new HashMap[Actor, String]

		// actor names attribute
		attributeNames("nodeName") = "d0"

		// write actors
		var nodes = new Queue[Node]
		for (val actor <- graph.getActors()) {
			// write attributes
			val attributes = writeAttributes(actor, attributeNames)

			// write actor
			val id = "n" + nodes.length
			nodes += <node id={id}>{attributes}</node>
			actorIDs(actor) = id
		}

		// write links
		var edges = new Queue[Node]
		for (val link <- graph.getLinks()) {
			// write attributes
			val attributes = writeAttributes(link, attributeNames)

			// write link
			val id = "e" + edges.length
			val from = actorIDs(link.from)
			val to = actorIDs(link.to)
			val directed = (link.direction == Unidirectional).toString
			edges += <edge id={id} source={from} target={to} directed={directed}>{attributes}</edge>
		}

		// attributes
		var attributes = new Queue[Node]
		for ((key, value) <- attributeNames)
			key match {
				case "nodeName" => attributes += <key for="node" id={value} yfiles.type="nodegraphics"/>
				case _ => attributes += <key attr.name={key}  attr.type="string" id={value} />
			}

		// save document
		val data = <graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
			{attributes}
			<graph edgedefault="directed" id="G">
				{nodes}
				{edges}
			</graph>
		</graphml>
		XML.save(file, data, "UTF-8", true)
	}

	private def readAttributes(elem: Node, attributes: HashMap[String,  String], target: GraphElement) {
		// read attributes
		for (val attr <- elem \\ "data") {
			val attrID = (attr \ "@key").text
			if (!attributes(attrID).isEmpty)
				target.setAttribute(attributes(attrID), attr.text)
		}

		// read extra position data
		target match {
			case actor: Actor => {
				val x = (elem \\ "Geometry" \ "@x").text
				val y = (elem \\ "Geometry" \ "@y").text
				if (actor.getAttribute("x").isEmpty)
					actor.setAttribute("x", if (x.isEmpty) "0" else x)
				if (actor.getAttribute("y").isEmpty)
					actor.setAttribute("y", if (y.isEmpty) "0" else y)
			}
			case _ =>
		}
	}

	private def writeAttributes(target: GraphElement, attributeNames: HashMap[String,  String]): Queue[Node] = {
		val attributes = new Queue[Node]

		// add attributes to list
		for ((key, value) <- target.getAttributes()) {
			if (!attributeNames.contains(key))
				attributeNames(key) = "d" + attributeNames.size

			val id = attributeNames(key)
			attributes +=  <data key={id}>{value}</data>
		}

		// add extra position data
		val id = attributeNames("nodeName")
		val x = target.getAttribute("x")
		val y = target.getAttribute("y")
		target match {
			case actor: Actor => if (!x.isEmpty || !y.isEmpty || !actor.name.isEmpty) {
				attributes += <data key="d0">
					<y:ShapeNode>
						<y:Geometry height="30.0" width="30.0" x={x} y={y} />
						<y:NodeLabel>{actor.name}</y:NodeLabel>
					</y:ShapeNode>
				</data>
			}
			case _ =>
		}

		attributes
	}
}
