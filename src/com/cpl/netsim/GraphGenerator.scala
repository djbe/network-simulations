package com.cpl.netsim

import com.cpl.netsim.graph._
import scala.collection.mutable.ArrayBuffer

object GraphGenerator {

	/**
	 * Returns a random {@link Graph} with <nbNodes> nodes and <nbEdges> <dir> edges
	 * @param nbNodes	The amount of {@link Actor} you wish in the graph
	 * @param nbEdges	The amount of {@link Link} you wish in the graph
	 * @param dir 		The preffered {@link Direction} of the {@link Link}s 
	 */
	def createRandomGraph(nbNodes:Int, nbEdges:Int, dir:Direction): Graph ={
		val graph:Graph = new Graph
		val actors: ArrayBuffer[Actor] = new ArrayBuffer[Actor]

		//create actors
		for (i <- 0 until nbNodes) {
			val actor:Actor = new Actor(graph, "Actor " + i)

			actor.setAttribute("x", RNG.randomInt(300).toString)
			actor.setAttribute("y", RNG.randomInt(300).toString)
			actors. += (actor)
		}

		for (j <- 0 until nbEdges) {
			val from = RNG.randomInt(nbNodes)
			var to = RNG.randomInt(nbNodes)

			if (from == to)
				to = (from + 1) % nbNodes

			actors(from).createLink(actors(to), dir)
		}

		graph
	}
  
	/**
	 * Returns a random {@link Graph} with <nbNodes> nodes and <nbEdges> <dir> edges
	 * @param nbNodes	The amount of {@link Actor} you wish in the graph
	 * @param edgeDensity	The density what the random of the graph should have
	 * @param dir 		The preferred {@link Direction} of the {@link Link}s 
	 */
	def createRandomGraph(nbNodes:Int, edgeDensity:Double, dir:Direction): Graph ={
		createRandomGraph(nbNodes, edgeDensity*nbNodes, dir)
  	}
}
