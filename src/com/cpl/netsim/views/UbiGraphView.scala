package com.cpl.netsim.views

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._
import org.ubiety.ubigraph.UbigraphClient
import scala.collection.mutable.HashMap

/**
 * Live viewer using the UbiGraph server, which needs to be running (it's a separate process). Should the server be
 * located at a custom location, pass the url to it as a parameter
 */

class UbiGraphView(server: String = "") extends View {
	private val graph = connectToServer()
	private val idMap = new HashMap[Object, Int]()

	override def filter(): List[Tag] = {
		List(TActor, TLink)
	}

	override def notify(tag: Tag, time: Int, author: Object, message: String, params: Map[String, String]) {
		tag match {
			case TActor => updateVertices(author, message, params)
			case TLink => updateEdges(author, message, params)
			case _ =>
		}
	}

	private def updateVertices(author: Object, message: String, params: Map[String, String]) {
		author match {
			case actor: Actor =>
				message match {
					case "created" =>
						val vertex = graph.newVertex()
						graph.setVertexAttribute(vertex, "label", actor.name)
						idMap(actor) = vertex
					case "deleted" =>
						graph.removeVertex(idMap(actor))
						idMap -= actor
					case "attribute.set" =>
						for ((name, value) <- mapAttribute(params("name"), actor, params("new")))
							graph.setVertexAttribute(idMap(actor), name, value)
					case "attribute.remove" =>
						for ((name, value) <- mapAttribute(params("name"), actor, params("new")))
							graph.setVertexAttribute(idMap(actor), name, value)
					case _ =>
				}
			case _ => throw new ClassCastException
		}
	}

	private def updateEdges(author: Object, message: String, params: Map[String, String]) {
		author match {
			case link: Link =>
				message match {
					case "created" =>
						val edge = graph.newEdge(idMap(link.from), idMap(link.to))
						idMap(link) = edge
					case "deleted" =>
						graph.removeEdge(idMap(link))
						idMap -= link
					case "attribute.set" =>
						for ((name, value) <- mapAttribute(params("name"), link, params("new")))
							graph.setVertexAttribute(idMap(link), name, value)
					case "attribute.remove" =>
						for ((name, value) <- mapAttribute(params("name"), link, params("new")))
							graph.setVertexAttribute(idMap(link), name, value)
					case _ =>
				}
			case _ => throw new ClassCastException
		}
	}

	private def connectToServer(): UbigraphClient = {
		var graph: UbigraphClient = null

		try {
			if (server.isEmpty)
				graph = new UbigraphClient()
			else
				graph = new UbigraphClient(server)
			graph.clear()
		} catch {
			case e: Exception =>
			    println("Error connecting to UbiGraph server: " + e)
				terminate()
		}

		graph
	}
}