package com.cpl.netsim.views

import com.cpl.netsim.monitoring._
import com.panayotis.gnuplot.JavaPlot
import com.panayotis.gnuplot.plot.DataSetPlot
import collection.mutable.{ListBuffer, HashMap}
import com.panayotis.gnuplot.style.Style

/**
 * View that plots statistics for the specified attribute values every time the show method is called
 * Receives in it's constructor at least one pair of strings which point to an attribute name and value
 */

class GnuPlotView(a: (String, String), b: (String, String)*) extends View {
	private val attributes = a +: b
	private val percentages = new HashMap[(String, String), ListBuffer[Int]]()
	private val plot: JavaPlot = new JavaPlot()

	// constructor
	for (key <- attributes)
		percentages(key) = ListBuffer(0)

	/**
	 * Show this view, i.e. plot the data in a graph
	 */
	def show() {
		val size = percentages.head._2.length

		// data array
		val data = new Array[Array[Int]](size)
		for (i <- (0 to size - 1)) {
			data(i) = new Array[Int](2)
			data(i)(0) = i
		}

		// for each registered attribute value
		for (key <- attributes) {
			for (i <- (0 to size - 1))
				data(i)(1) = percentages(key)(i)


			val temp = new DataSetPlot(data)
			temp.setTitle(key._1 + "=" + key._2)
			temp.getPlotStyle.setStyle(Style.LINESPOINTS)
			plot.addPlot(temp)
		}

		plot.plot()
	}

	override def filter(): List[Tag] = {
		List(TStatistic, TEvent, TSimulation)
	}

	override def notify(tag: Tag, time: Int, author: Object, message: String, params: Map[String, String]) {
		tag match {
			case TStatistic =>
				val key = (author.toString, params("name"))
				if (attributes.contains(key))
					percentages(key)(percentages(key).length - 1) = params("new").toInt
			case TEvent =>
				for (key <- attributes)
					percentages(key) += percentages(key).last
			case TSimulation =>
				for (key <- attributes)
					percentages(key) += percentages(key).last
		}
	}
}