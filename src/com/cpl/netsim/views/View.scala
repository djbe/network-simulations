package com.cpl.netsim.views

import com.cpl.netsim.graph._
import com.cpl.netsim.monitoring._
import collection.mutable.HashMap

/**
 * Trait common to all viewers
 * @author David Jennes
 */

trait View extends Monitor {
	protected val mappers = new HashMap[String, (GraphElement, String) => Map[String, String]]()

	def setAttributeMapper(attribute: String, mapper: (GraphElement, String) => Map[String, String]) {
		mappers(attribute) = mapper
	}

	def removeAttributeMapper(attribute: String) {
		mappers -= attribute
	}

	protected def mapAttribute(attribute: String, author: GraphElement, value: String): Map[String, String] = {
		if (mappers.contains(attribute))
			mappers(attribute)(author, value)
		else
			Map()
	}
}
