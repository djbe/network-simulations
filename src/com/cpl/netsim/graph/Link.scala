package com.cpl.netsim.graph

import com.cpl.netsim.monitoring._

/**
 * Links represent the edges between actors (nodes) in a graph
 */

sealed trait Direction
case object Unidirectional extends Direction
case object Bidirectional extends Direction

class Link(g: Graph,  actorFrom: Actor, actorTo: Actor, dir: Direction) extends GraphElement {
	def to: Actor = actorTo;
	def from: Actor = actorFrom;
	def direction: Direction = dir;
	override def graph: Graph = g

	// constructor
	from.addLink(this)
	to.addLink(this)
	graph.addLink(this)
	Log.logObject(TLink, this, "created");

	/**
	 * TODO: Commentaar
	 */
  	def delete() {
    	to.removeLink(this);
    	from.removeLink(this);
    	Log.logObject(TLink, this, "deleted");
  	}

  	/**
  	 * @see super.delete()
  	 */
	override def setAttribute(name: String, value: String) {
		Log.logObjectNamedChange(TLink, this, "attribute.set", name, getAttribute(name), value)
		super.setAttribute(name, value)
	}

	/**
	 * @see super.delete()
	 */
	override def removeAttribute(name: String) {
		Log.logObjectValue(TLink, this, "attribute.remove", name)
		super.removeAttribute(name)
	}
}
