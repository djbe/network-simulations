package com.cpl.netsim.graph

import collection.mutable.HashMap

/**
 * Trait common to both Actor and Link classes
 * @author David Jennes
 */

trait GraphElement {
	private val attributes: HashMap[String, String] = new HashMap[String, String]
	def graph: Graph

	/**
	 * Check if a certain attribute is set
	 */
	def hasAttribute(name: String): Boolean = {
		attributes.contains(name)
	}

	/**
	 * Get the value of a certain attribute or an empty string if it isn't set
	 */
	def getAttribute(name: String): String = {
		if (attributes.contains(name))
			attributes(name)
		else
			""
	}

	/**
	 * Set the value of a certain attribute
	 */
	def setAttribute(name: String, value: String) {
		attributes(name) = value
	}

	/**
	 * Remove a certain attribute from the element
	 */
	def removeAttribute(name: String) {
		attributes -= name
	}

	def getAttributes() = attributes
}