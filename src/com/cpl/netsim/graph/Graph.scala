package com.cpl.netsim.graph

import scala.collection.mutable.ListBuffer
import com.cpl.netsim.monitoring._
import com.cpl.netsim.RNG

class Graph {
	Log.logObject(TGraph, this, "created");
	private val actors: ListBuffer[Actor] = new ListBuffer[Actor]();
	private val links: ListBuffer[Link] = new ListBuffer[Link]();

	def addActor(actor: Actor) {
		actors. += (actor)
	}
	
	def removeActor(actor: Actor) {
		actors. -= (actor)
		actor.delete()
	}

	def actorsCount() = {
		actors.length
	}

	def getActors() = {
		actors
	}

	def addLink(link: Link) {
		links. += (link)
	}

	def removeLink(link: Link) {
		links. -= (link)
		link.delete()
	}

	def linksCount() = {
		links.length
	}

	def getLinks() = {
		links
	}

	/**
	 * Get a random actor
	 */
	def getRandomActor(): Actor = {
		actors(RNG.randomInt(actorsCount()))
	}

	/**
	 * Get a random link
	 */
	def getRandomLink(): Link = {
		links(RNG.randomInt(linksCount()))
	}

	/**
	 * Find all actors in the graph with a certain attribute
	 */
	def getActorsWithAttribute(attribute : String): ListBuffer[Actor] = {
		var list: ListBuffer[Actor] = new ListBuffer[Actor]

		for (actor <- actors)
			if (actor.hasAttribute(attribute))
		    	list += actor

		list
	}

	/**
	 * Find all actors in the graph with a certain attribute value
	 * @param attribute The attribute name
	 * @param value The attribute value
	 */
	def getActorsWithAttributeValue(attribute: String, value: String): ListBuffer[Actor] = {
		var list: ListBuffer[Actor] = new ListBuffer[Actor]

		for (actor <- actors)
			if (value.equals(actor.getAttribute(attribute)))
		    	list += actor

		list
	}

	/**
	 * Find all links in the graph with a certain attribute
	 */
	def getLinkWithAttribute(attribute : String): ListBuffer[Link] = {
		var list: ListBuffer[Link] = new ListBuffer[Link]

		for (link <- links)
			if (link.hasAttribute(attribute))
		    	list += link

		list
	}

	/**
	 * Find all links in the graph with a certain attribute value
	 * @param attribute The attribute name
	 * @param value The attribute value
	 */
	def getLinkWithAttributeValue(attribute : String, value: String): ListBuffer[Link] = {
		var list: ListBuffer[Link] = new ListBuffer[Link]

		for (link <- links)
			if (value.equals(link.getAttribute(attribute)))
		    	list += link

		list
	}
	
	/**
	 * TODO: commentaar
	 */
	def getBreadthFirstTraversal(from: Actor): ListBuffer[Actor] = {
		val alreadyChecked: ListBuffer[Actor] = new ListBuffer[Actor]();

		alreadyChecked. += (from)
		var traversalList: ListBuffer[Actor] = checkActorsBreadthFirst(from, alreadyChecked)

		traversalList;
	}
	
	private def checkActorsBreadthFirst(from: Actor, alreadyChecked: ListBuffer[Actor]): ListBuffer[Actor] = {
		var linksFromActor: List[Actor] = from.neighbours(1);
		val notAlreadyCheckedLinksFromActor = removeAlreadyChecked(linksFromActor, alreadyChecked)

		for (neighbour <- notAlreadyCheckedLinksFromActor)
			alreadyChecked. += (neighbour)
		for (neighbour <- notAlreadyCheckedLinksFromActor)
			checkActorsBreadthFirst(neighbour, alreadyChecked)

		alreadyChecked
	}
	
	private def removeAlreadyChecked(listToCheck: List[Actor], alreadyChecked: ListBuffer[Actor]): ListBuffer[Actor] = {
		val alreadyChecked2 = alreadyChecked

		for (checked <- alreadyChecked)
			for (toCheck <- listToCheck)
		  		if (checked == toCheck)
					alreadyChecked2.-=(toCheck)

		alreadyChecked2
	}
	
	/**
	 * TODO: Commentaar
	 */
	def getDepthFirstTraversal(from: Actor): ListBuffer[Actor] = {
		val alreadyChecked: ListBuffer[Actor] = new ListBuffer[Actor]();

		alreadyChecked. += (from)
		var traversalList: ListBuffer[Actor] = checkActorsDepthFirst(from, alreadyChecked)

		traversalList;
	}
	
	private def checkActorsDepthFirst(from: Actor, alreadyChecked: ListBuffer[Actor]): ListBuffer[Actor] = {
		val linksFromActor: List[Actor] = from.neighbours(1);
		val notAlreadyCheckedLinksFromActor = removeAlreadyChecked(linksFromActor, alreadyChecked)

		for (neighbour <- notAlreadyCheckedLinksFromActor) {
			alreadyChecked. += (neighbour)
			checkActorsDepthFirst(neighbour, alreadyChecked)
		}

		alreadyChecked
	}
}
