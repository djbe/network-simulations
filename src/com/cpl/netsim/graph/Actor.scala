package com.cpl.netsim.graph

import com.cpl.netsim.monitoring._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer

/**
 * Actors represent nodes in a graph
 */

class Actor(g: Graph, actorName: String) extends GraphElement {
	private var links: ArrayBuffer[Link] = new ArrayBuffer[Link]();
	def name: String = actorName;
	override def graph: Graph = g

	// constructor
	graph.addActor(this)
	Log.logObject(TActor, this, "created");

	override def toString: String = name

	/**
	 * Find all the neighbouring actors (search up to a certain depth)
	 */
	def neighbours(depth: Int = 1): List[Actor] = {
		if (depth == 0)
	    	List()
	  	else if (depth == 1) {
	    	var list: ListBuffer[Actor] = new ListBuffer[Actor]()

	    	for (link: Link <-links) {
	    		if (link.from == this)
	        		list. += (link.to)
	      		if (link.to == this && link.direction == Bidirectional)
	        		list. += (link.from);
	    	}

	    	list.toList
	  	} else
	    	expand(neighbours(depth - 1))
	}
	
	def getLinks(): List[Link]={
	  return links.toList
	}

	/**
	 * Create a new link to a given actor
	 * @param dir The direction of the link
	 */
	def createLink(to: Actor, dir: Direction): Link = {
//		Log.logObject(TLink, this, "Creating edge: %s with direction %s", to, dir);
		new Link(graph, this, to, dir)
	}

	def delete() {
		for (link: Link <- links.clone())
			link.delete()

		Log.logObject(TActor, this, "deleted");
	}
		
	def addLink(link: Link) {
		links. += (link);
		//Log.logObject(TLink, this, "Link added: %s", link);
	}
	
	def removeLink(link: Link) {
		links. -= (link);
		//Log.logObject(TLink, this, "Link removed: %s", link);
	}
	
	private def expand(list: List[Actor]): List[Actor] ={
		val listExp: ListBuffer[Actor] = new ListBuffer[Actor]

		for(actor: Actor <- list) {
	    	if(!listExp.contains(actor))
	      		listExp. += (actor)

	    	for (neighbour: Actor <- actor.neighbours())
	      		if (!listExp.contains(neighbour))
	        		listExp. += (neighbour)
	  	}
	  
	  	listExp.toList;
	}

	/**
	 * @See super.setAttribute(name: String, value: String)
	 */
	override def setAttribute(name: String, value: String) {
		Log.logObjectNamedChange(TActor, this, "attribute.set", name, getAttribute(name), value)
		super.setAttribute(name, value)
	}

	override def removeAttribute(name: String) {
		Log.logObjectValue(TActor, this, "attribute.remove", name)
		super.removeAttribute(name)
	}
}
