package com.cpl.netsim

import scala.util.Random
import collection.generic.CanBuildFrom

/**
 * Random number generator, must be used in simulations
 */
object RNG {
	private var seed: Int = 10
	private var rng: Random = new Random(seed)

	/**
	 * Reset the RNG with a new seed value
	 */
	def reseed(s: Int) {
		seed = s
		rng = new Random(seed)
	}

	/**
	 * Generate a random integer with a max. value
	 */
	def randomInt(max: Int): Int = {
		rng.nextInt(max)
	}

	/**
	 * Generate a random double between 0 and 1
	 */
	def random: Double = {
		rng.nextDouble()
	}

	/**
	 * Shuffle a list
	 */
	def shuffle[T, CC[X] <: TraversableOnce[X]](list: CC[T])(implicit bf: CanBuildFrom[CC[T], T, CC[T]]): CC[T] = {
	    rng.shuffle(list)
	}
}
